#include <snowflake/id/impl/matrix.hpp>
#include <snowflake/random/impl/twister.hpp>

#include <cassert>
#include <iostream>

void MatrixIdGeneratorTest() {
  snowflake::id::matrix::Generator ids;

  auto id1 = ids.GenerateId();
  auto id2 = ids.GenerateId();

  std::cout << "id1 = " << id1 << std::endl;
  std::cout << "id2 = " << id2 << std::endl;

  assert(id1 != id2);

  std::cout << "MatrixIdGeneratorTest: OK" << std::endl;
}

void MersenneTwisterTest() {
  snowflake::random::Twister twister{/*seed=*/42};

  static const size_t kSize = 17;
  static const size_t kIterations = 12345;


  for (size_t i = 0; i < kIterations; ++i) {
    uint64_t index = twister.Choice(kSize);

    assert(index < kSize);
  }

  std::cout << "MersenneTwisterTest: OK" << std::endl;
}

int main() {
  MatrixIdGeneratorTest();
  MersenneTwisterTest();
  return 0;
}
