#pragma once

#include <snowflake/id/generator.hpp>

#include <cstdint>
#include <sstream>

namespace snowflake::id::matrix {

// Id generator implementation for deterministic simulation

class Generator : public IGenerator {
 public:
  id::Id GenerateId() override {
    std::stringstream out;
    out << "id-" << ++next_id_;
    return out.str();
  }

 private:
  size_t next_id_{0};
};

}  // namespace snowflake::id::matrix
