#pragma once

#include <snowflake/id/id.hpp>

namespace snowflake::id {

struct IGenerator {
  virtual ~IGenerator() = default;

  // Generates globally unique identifier
  virtual Id GenerateId() = 0;
};

}  // namespace snowflake::id
