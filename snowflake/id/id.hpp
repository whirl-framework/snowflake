#pragma once

#include <string>

namespace snowflake::id {

using Id = std::string;

}  // namespace snowflake::id
