#include <snowflake/random/impl/twister.hpp>

namespace snowflake::random {

static uint64_t RandomSeed() {
  return std::random_device{}();
}

Twister::Twister()
  : Twister(RandomSeed()) {
}

Twister::Twister(uint64_t seed)
  : impl_(seed) {
}

}  // namespace snowflake::random
