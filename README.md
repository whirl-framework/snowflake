# Snowflake

Unique ID and random number generation

## Contents

- [`id::IGenerator`](snowflake/id/generator.hpp)
- [`random::IGenerator`](snowflake/random/generator.hpp)
