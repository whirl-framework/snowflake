#pragma once

#include <snowflake/random/generator.hpp>

#include <random>

namespace snowflake::random {

class Twister : public IGenerator {
 public:
  Twister();

  Twister(uint64_t seed);

  uint64_t Jitter(uint64_t bound) override {
    return impl_() % bound;
  }

  size_t Choice(size_t alts) override {
    return impl_() % alts;
  }

 private:
  std::mt19937_64 impl_;
};

}  // namespace snowflake::random
