#pragma once

#include <cstdint>
#include <cstdlib>

#include <chrono>

namespace snowflake::random {

struct IGenerator {
  virtual ~IGenerator() = default;

  // For timeouts

  // Precondition: bound > 0
  // Returns random delay in [0, bound]
  virtual uint64_t Jitter(uint64_t bound) = 0;

  // For selection

  // Returns random index in [0, alts)
  virtual size_t Choice(size_t alts) = 0;
};

}  // namespace snowflake::random
